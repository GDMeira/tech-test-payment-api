using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Sale
    {
        
        public Sale(int id, Salesman saleMaker, DateTime date, string vendas) 
        {
            this.Id = id;
            this.SaleMaker = saleMaker;
            this.Date = date;
            this.Vendas = vendas;
   
        }
        public int Id { get; }

        public Salesman SaleMaker { get; }

        public DateTime Date { get;  }

        public string Vendas { get; }

        //TODO: criar atributo status

    }
}