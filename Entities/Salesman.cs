using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Salesman
    {
        public Salesman(int id, string cpf, string name, string phoneNumber) 
        {
            this.Id = id;
            this.Cpf = cpf;
            this.Name = name;
            this.PhoneNumber = phoneNumber;
   
        }
        public int Id { get; set; }

        public string Cpf { get; }

        public string Name { get; }

        public string PhoneNumber { get; set; }
        
    }
}